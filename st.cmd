require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

require magnetps, 1.1.1+0
require fug, 3.1.3+1 		 
require sairem, 3.1.1+1
require caenelsmagnetps, 1.2.2+1
require tdklambdagenesys, 1.3.2+0

epicsEnvSet(ISRC_P, "ISrc-CS:")
epicsEnvSet(LEBT_P, "LEBT-CS:")

# FUG @ HCH 15k-100k [High Voltage Power Supply] 
epicsEnvSet(HVPS_R, "ISS-HVPS-01:")
epicsEnvSet(HVPS_HOST, "isrc-hvps.tn.esss.lu.se")
epicsEnvSet(HVPS_CHANNEL, "HVPS")

iocshLoad("$(fug_DIR)/fug_HV.iocsh", "Ch_name=$(HVPS_CHANNEL), IP_addr=$(HVPS_HOST), P=$(ISRC_P), R=$(HVPS_R), EGU=kV, ASLO=0.1")

# FUG @HCP 35-3500 [Repeller Power Supply]
epicsEnvSet(REP01_R, "PwrC-PSRep-01:")
epicsEnvSet(REP01_HOST, "isrc-repps.tn.esss.lu.se")
epicsEnvSet(REP01_CHANNEL, "RepPS-01")

iocshLoad("$(fug_DIR)/fug_HV.iocsh", "Ch_name=$(REP01_CHANNEL), IP_addr=$(REP01_HOST), P=$(ISRC_P), R=$(REP01_R), EGU=kV, ASLO=0.1")

# TDK Lambda Genesys 10-500 [Coils] 
epicsEnvSet(COIL01_R, "PwrC-PSCoil-01:")
epicsEnvSet(COIL01_HOST, "isrc-coilps1.tn.esss.lu.se")
epicsEnvSet(COIL01_CHANNEL, "CoilsPS-01")

iocshLoad("$(tdklambdagenesys_DIR)/isrc-coils.iocsh", "CONNECTIONNAME=$(COIL01_CHANNEL), IP_ADDR=$(COIL01_HOST), P=$(ISRC_P), R=$(COIL01_R), IP_PORT=8003, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12")
dbLoadRecords("db/ObserveVoltageLevels.template","P=$(ISRC_P), R=$(COIL01_R)")

epicsEnvSet(COIL02_R, "PwrC-PSCoil-02:")
epicsEnvSet(COIL02_HOST, "isrc-coilps2.tn.esss.lu.se")
epicsEnvSet(COIL02_CHANNEL, "CoilsPS-02")

iocshLoad("$(tdklambdagenesys_DIR)/isrc-coils.iocsh", "CONNECTIONNAME=$(COIL02_CHANNEL), IP_ADDR=$(COIL02_HOST), P=$(ISRC_P), R=$(COIL02_R), IP_PORT=8003, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12")
dbLoadRecords("db/ObserveVoltageLevels.template","P=$(ISRC_P), R=$(COIL02_R)")

epicsEnvSet(COIL03_R, "PwrC-PSCoil-03:")
epicsEnvSet(COIL03_HOST, "isrc-coilps3.tn.esss.lu.se")
epicsEnvSet(COIL03_CHANNEL, "CoilsPS-03")

iocshLoad("$(tdklambdagenesys_DIR)/isrc-coils.iocsh", "CONNECTIONNAME=$(COIL03_CHANNEL), IP_ADDR=$(COIL03_HOST), P=$(ISRC_P), R=$(COIL03_R), IP_PORT=8003, MAXVOL=10, MAXCUR=500, HICUR=400, HIHICUR=490, MAXOVP=12")
dbLoadRecords("db/ObserveVoltageLevels.template","P=$(ISRC_P), R=$(COIL03_R)")

# Sairem @ GMP20KED [Magnetron]
epicsEnvSet(MAGTR_R, "ISS-Magtr-01:")
epicsEnvSet(MAGTR_HOST, "isrc-magtr.tn.esss.lu.se")
epicsEnvSet(MAGTR_CHANNEL, "Magtr")

iocshLoad("$(sairem_DIR)/sairem_Magnetron_RfGen.iocsh", "PORT_NAME=$(MAGTR_CHANNEL), IP_ADDR=$(MAGTR_HOST),P=$(ISRC_P), R=$(MAGTR_R), SLAVE_ADDR=1, MAX_PWR_W=2000")

dbLoadRecords("db/Magnetron_watchdog.template","P=$(ISRC_P), R=$(MAGTR_R), General_Fault_PV=ISrc-CS:ISS-Magtr-01:GenFault")

# Sairem @ AI4S [ATU] 
epicsEnvSet(ATU_R, "ISS-ATU-01:")
epicsEnvSet(ATU_HOST, "isrc-atu.tn.esss.lu.se")
epicsEnvSet(ATU_PORT, "ATU")

iocshLoad("$(sairem_DIR)/sairem_ATU_Tuner.iocsh", "P=$(ISRC_P), R=$(ATU_R), PORT_NAME=$(ATU_PORT), IP_ADDR=$(ATU_HOST), SLAVE_ADDR=0")

# FUG @HCP 35-3500 [Repeller Power Supply]
epicsEnvSet(REP_R, "PwrC-PSRep-01:")
epicsEnvSet(REP_HOST, "lebt-repps.tn.esss.lu.se")
epicsEnvSet(REP_CHANNEL, "RepPS-02")

iocshLoad("$(fug_DIR)/fug_HV.iocsh", "Ch_name=$(REP_CHANNEL), IP_addr=$(REP_HOST), P=$(LEBT_P), R=$(REP_R),EGU=kV,ASLO=0.1")

# Caen @ Fast-Ps [STEERERS]

epicsEnvSet(STR01H_CONVERSIONTABLE, "LEBT-010_BMD-CH-01")
epicsEnvSet(STR02H_CONVERSIONTABLE, "LEBT-010_BMD-CH-02")
epicsEnvSet(STR01V_CONVERSIONTABLE, "LEBT-010_BMD-CV-01")
epicsEnvSet(STR02V_CONVERSIONTABLE, "LEBT-010_BMD-CV-02")

# Load conversion break point table

dbLoadDatabase("$(STR01H_CONVERSIONTABLE).dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("$(STR02H_CONVERSIONTABLE).dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("$(STR01V_CONVERSIONTABLE).dbd", "$(magnetps_DB)/", "")
dbLoadDatabase("$(STR02V_CONVERSIONTABLE).dbd", "$(magnetps_DB)/", "")
updateMenuConvert

# H1 
epicsEnvSet(STR01H_R, "PwrC-PSCH-01:")
epicsEnvSet(STR01H_HOST, "lebt-strps1h.tn.esss.lu.se")
epicsEnvSet(STR01H_CHANNEL, "PwrC-PSCH-01")

iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=$(STR01H_CHANNEL), IP_ADDR=$(STR01H_HOST), P=$(LEBT_P), R=$(STR01H_R), MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=$(LEBT_P), R=$(STR01H_R), READCURPV=Cur-R, BPT=$(STR01H_CONVERSIONTABLE), EGU=T, ADEL=0")

# H2
epicsEnvSet(STR02H_R, "PwrC-PSCH-02:")
epicsEnvSet(STR02H_HOST, "lebt-strps2h.tn.esss.lu.se")
epicsEnvSet(STR02H_CHANNEL, "PwrC-PSCH-02")

iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=$(STR02H_CHANNEL), IP_ADDR=$(STR02H_HOST), P=$(LEBT_P), R=$(STR02H_R), MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=$(LEBT_P), R=$(STR02H_R), READCURPV=Cur-R, BPT=$(STR02H_CONVERSIONTABLE), EGU=T, ADEL=0")

# V1
epicsEnvSet(STR01V_R, "PwrC-PSCV-01:")
epicsEnvSet(STR01V_HOST, "lebt-strps1v.tn.esss.lu.se")
epicsEnvSet(STR01V_CHANNEL, "PwrC-PSCV-01")

iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=$(STR01V_CHANNEL), IP_ADDR=$(STR01V_HOST), P=$(LEBT_P), R=$(STR01V_R), MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=$(LEBT_P), R=$(STR01V_R), READCURPV=Cur-R, BPT=$(STR01V_CONVERSIONTABLE), EGU=T, ADEL=0")

# V2
epicsEnvSet(STR02V_R, "PwrC-PSCV-02:")
epicsEnvSet(STR02V_HOST, "lebt-strps2v.tn.esss.lu.se")
epicsEnvSet(STR02V_CHANNEL, "PwrC-PSCV-02")

iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=$(STR02V_CHANNEL), IP_ADDR=$(STR02V_HOST), P=$(LEBT_P), R=$(STR02V_R), MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")
dbLoadRecords("$(magnetps_DB)/magnetps.template", "P=$(LEBT_P), R=$(STR02V_R), READCURPV=Cur-R, BPT=$(STR02V_CONVERSIONTABLE), EGU=T, ADEL=0")

epicsEnvSet(STREAM_PROTOCOL_PATH, "$(tdklambdagenesys_DB):$(caenelsmagnetps_DB):$(fug_DB):$(sairem_DB)")

dbLoadRecords("db/Operational_Parameters.db")

iocInit()
